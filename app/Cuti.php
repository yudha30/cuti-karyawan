<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Karyawan;
use App\Jenis_Cuti;

class Cuti extends Model
{
    //
    protected $table='cuti';
    protected $fillable=[
        'nik',
        'nama',
        'tanggal_cuti',
        'tanggal_masuk',
        'jumlah_cuti',
        'jenis_cuti',
        'keperluan',
        'periode',
        'status'
    ];

    public function karyawan()
    {
        return $this->belongsTo(Karyawan::class);
    }

    public function jenis_cuti()
    {
        return $this->belongsTo(Jenis_Cuti::class);
    }
}
