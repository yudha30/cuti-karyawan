<?php

namespace App;
use App\Karyawan;

use Illuminate\Database\Eloquent\Model;

class Departemen extends Model
{
    //
    protected $table ='departemen';
    protected $fillable=[
        'departemen'
    ];

    public function karyawan()
    {
        return $this->hasOne(Karyawan::class);
    }
}
