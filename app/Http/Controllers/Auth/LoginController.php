<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;
use Auth;
use Illuminate\Http\Request;
use App\Karyawan;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function cek(Request $request){
        $user=User::where('email',$request->email)->first();
        if($user)
        {
            if (Hash::check($request->password, $user->password)) {
                Auth::guard('web')->login($user, true);
                return redirect('/home');
            }
            else{
                return redirect()->back()->with('status', 'Data Email Atau Password Tidak Ditemukan');
            }
        }
        else{
            $karyawan = Karyawan::where('email',$request->email)->first();
            if($karyawan)
            {
                if (Hash::check($request->password, $karyawan->password)) {
                    Auth::guard('karyawans')->login($karyawan, true);
                    return redirect('/karyawan/home_karyawan');
                }
                else{
                    return redirect()->back()->with('status', 'Data Email Atau Password Tidak Ditemukan');
                }
            }
            else{
                return redirect()->back()->with('status', 'Data Email Atau Password Tidak Ditemukan');
            }
        }

    }
    public function logout()
    {
        if (Auth::guard('web')->check())
        {
            Auth::guard('web')->logout();
            return redirect('/login');
        }
        else
        {
            Auth::guard('karyawans')->logout();
            return redirect('/login');
        }
    }


}
