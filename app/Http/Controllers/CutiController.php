<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Cuti;
use App\Mail\NotifCuti;
use App\Mail\NotifCutiSupervisor;
use App\Karyawan;
use App\Jenis_Cuti;
use App\Jabatan;
use App\Departemen;
use Illuminate\Support\Facades\DB;
use PDF;

class CutiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_cuti=Cuti::orderBy('id','desc')->paginate(10);
        $count=0;
        $count=count($data_cuti);
        return view ('/hrd/cuti',compact('data_cuti','count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $data_cuti=Cuti::where('nama','LIKE','%'.$request->search.'%')
        ->orWhere('kode_rahasia','LIKE','%'.$request->search.'%')
        ->paginate(10);
        $count=0;
        $count=count($data_cuti);
        return view ('/hrd/cuti',compact('data_cuti','count'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $jenis_cuti=Jenis_Cuti::all();
        $cuti=Cuti::find($id);
        return view('/hrd/detail_cuti', compact('cuti','jenis_cuti'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data_cuti = Cuti::find($id);
        $data_cuti->status = 'Diterima';
        $data_cuti->save();
        $email=$data_cuti->karyawan->email;
        $data = array(
            'nama' => $data_cuti->karyawan->name,
            'status' => $data_cuti->status,
            'id_cuti' => $data_cuti->id,
            'id_karyawan' => $data_cuti->karyawan_id
        );

        //kirim notifikasi email
        Mail::to($email)->send(new NotifCuti($data));

        return redirect()->back()->with('sukses', 'Data Cuti Diterima');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function data_grafik($id)
    {
        $tahun = date('Y');
        for ($i=1; $i < 13; $i++) { 
            $data_cuti=Cuti::whereMonth('tanggal_cuti', $i)
            ->where('periode', $tahun)
            ->get();
            $jumlah=count($data_cuti);
        }

        dd($data_cuti);
        return view('/home', compact('data_cuti','jumlah'));
    
    }

    public function tolak_cuti($id){
        // cari cuti berdasarkan id
        $data_cuti = Cuti::find($id);

        // get data jumlah cuti kemudian diubah menjadi integer
        $jumlahCuti = (int)$data_cuti->jumlah_cuti;

        // update status cuti
        $data_cuti->status = 'Ditolak';
        $data_cuti->save();

        // cari karyawan berdasarkan id cuti
        $karyawan = Karyawan::find($data_cuti->karyawan_id);

        // get data sisa cuti kemudian diubah menjadi integer
        $sisa_cuti = (int)$karyawan->sisa_cuti;

        // jumlahkan data sisa cuti dengan jumlah cuti
        $karyawan->sisa_cuti = $sisa_cuti+$jumlahCuti;
        $karyawan->save();

        $email=$data_cuti->karyawan->email;
        $data = array(
            'nama' => $data_cuti->karyawan->name,
            'status' => $data_cuti->status,
            'id_cuti' => $data_cuti->id,
            'id_karyawan' => $data_cuti->karyawan_id
        );

        //kirim notifikasi email
        Mail::to($email)->send(new NotifCuti($data));

        return redirect()->back()->with('sukses','Data Cuti Ditolak');
    }

    public function download_file_cuti($id)
    {
        $findCuti = Cuti::find($id);
        if($findCuti->kode_rahasia == null || $findCuti->kode_rahasia == '')
        {
            $pool = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $passRand = substr(str_shuffle(str_repeat($pool, 9)), 0, 10);
            $findCuti->kode_rahasia=$passRand;
            $findCuti->save();

            //load pdf
            $pdf = PDF::loadview('cetakFileCuti.cetak',compact('findCuti'));
            return $pdf->download($passRand.'.pdf');
        }
        else{
            //load pdf
            $pdf = PDF::loadview('cetakFileCuti.cetak',compact('findCuti'));
            return $pdf->download($findCuti->kode_rahasia.'.pdf');
        }
    }

    public function acc_leader($id_cuti)
    {
        $findCuti = Cuti::find($id_cuti);
        $findCuti->status = 'ACC Leader';
        $findCuti->save();

        // find Supervisor
        $findSpv = DB::table('karyawan')
        ->join('jabatan','karyawan.jabatan_id','=','jabatan.id')
        ->where('jabatan.jabatan','SUPERVISOR')
        ->where('karyawan.departemen_id',$findCuti->karyawan->departemen->id)
        ->select('karyawan.email')
        ->first();


        $email=$findSpv->email;
        $data=array(
            'emailSpv'=> $email,
            'id_cuti'=>$id_cuti,
            'nama'=>$findCuti->karyawan->name,
            'departemen'=>$findCuti->karyawan->departemen->departemen,
            'tanggal_cuti' => $findCuti->tanggal_cuti,
            'tanggal_masuk' => $findCuti->tanggal_masuk,
            'lama_cuti'=> $findCuti->jumlah_cuti,
            'keperluan' => $findCuti->keperluan,
            'periode' => $findCuti->periode,
            'status' => $findCuti->status
        );

        //kirim notifikasi email ke Supervisor
        Mail::to($email)->send(new NotifCutiSupervisor($data));
        return view('notif.popupsuccess');

    }

    public function acc_supervisor($id_cuti)
    {
        $findCuti = Cuti::find($id_cuti);
        $findCuti->status = 'ACC Supervisor';
        $findCuti->save();

        //notifikasi pengajuan cuti ke dahsboard hrd
        return redirect('/karyawan/notifikasi/pengajuan_cuti');
    }

    public function leader_tolak_cuti($id)
    {
        $findCuti = Cuti::find($id);
        // get data jumlah cuti kemudian diubah menjadi integer
        $jumlahCuti = (int)$findCuti->jumlah_cuti;
        $findCuti->status = 'Ditolak';
        $findCuti->save();

        // cari karyawan berdasarkan id cuti
        $karyawan = Karyawan::find($findCuti->karyawan_id);

        // get data sisa cuti kemudian diubah menjadi integer
        $sisa_cuti = (int)$karyawan->sisa_cuti;

        // jumlahkan data sisa cuti dengan jumlah cuti
        $karyawan->sisa_cuti = $sisa_cuti+$jumlahCuti;
        $karyawan->save();

        $email=$findCuti->karyawan->email;
        $data = array(
            'nama' => $findCuti->karyawan->name,
            'status' => $findCuti->status,
            'id_cuti' => $findCuti->id,
            'id_karyawan' => $findCuti->karyawan_id
        );

        //kirim notifikasi email ke Karyawan
        Mail::to($email)->send(new NotifCuti($data));
        return view('notif.popupditolak');
    }

    public function spv_tolak_cuti($id_cuti)
    {
        $findCuti = Cuti::find($id_cuti);
        // get data jumlah cuti kemudian diubah menjadi integer
        $jumlahCuti = (int)$findCuti->jumlah_cuti;
        $findCuti->status = 'Ditolak';
        $findCuti->save();

        // cari karyawan berdasarkan id cuti
        $karyawan = Karyawan::find($findCuti->karyawan_id);

        // get data sisa cuti kemudian diubah menjadi integer
        $sisa_cuti = (int)$karyawan->sisa_cuti;

        // jumlahkan data sisa cuti dengan jumlah cuti
        $karyawan->sisa_cuti = $sisa_cuti+$jumlahCuti;
        $karyawan->save();

        $email=$findCuti->karyawan->email;
        $data = array(
            'nama' => $findCuti->karyawan->name,
            'status' => $findCuti->status,
            'id_cuti' => $findCuti->id,
            'id_karyawan' => $findCuti->karyawan_id
        );

        //kirim notifikasi email ke Karyawan
        Mail::to($email)->send(new NotifCuti($data));
        return view('notif.popupditolak');
    }
}
