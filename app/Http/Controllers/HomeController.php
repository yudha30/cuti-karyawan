<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Karyawan;
use App\Cuti;
use App\User;
use App\Departemen;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // jumlah karyawan
        $findKaryawan=Karyawan::all();
        $karyawan=0;
        $karyawan=count($findKaryawan);

        // jumlah cuti
        $findCuti = Cuti::all();
        $cuti=0;
        $cuti=count($findCuti);

        // jumlah hrd
        $findHRD = User::all();
        $hrd=0;
        $hrd=count($findHRD);

        // jumlah departemen
        $findDepartemen=Departemen::all();
        $departemen=0;
        $departemen=count($findDepartemen);

        $tahun = date('Y');
            $data_cuti1=Cuti::whereMonth('tanggal_cuti', '1')
            ->where('periode', $tahun)
            ->get();
            $jumlah1=count($data_cuti1);
            $data_cuti2=Cuti::whereMonth('tanggal_cuti', '2')
            ->where('periode', $tahun)
            ->get();
            $jumlah2=count($data_cuti2);
            $data_cuti3=Cuti::whereMonth('tanggal_cuti', '3')
            ->where('periode', $tahun)
            ->get();
            $jumlah3=count($data_cuti3);
            $data_cuti4=Cuti::whereMonth('tanggal_cuti', '4')
            ->where('periode', $tahun)
            ->get();
            $jumlah4=count($data_cuti4);
            $data_cuti5=Cuti::whereMonth('tanggal_cuti', '5')
            ->where('periode', $tahun)
            ->get();
            $jumlah5=count($data_cuti5);
            $data_cuti6=Cuti::whereMonth('tanggal_cuti', '6')
            ->where('periode', $tahun)
            ->get();
            $jumlah6=count($data_cuti6);
            $data_cuti7=Cuti::whereMonth('tanggal_cuti', '7')
            ->where('periode', $tahun)
            ->get();
            $jumlah7=count($data_cuti7);
            $data_cuti8=Cuti::whereMonth('tanggal_cuti', '8')
            ->where('periode', $tahun)
            ->get();
            $jumlah8=count($data_cuti8);
            $data_cuti9=Cuti::whereMonth('tanggal_cuti', '9')
            ->where('periode', $tahun)
            ->get();
            $jumlah9=count($data_cuti9);
            $data_cuti10=Cuti::whereMonth('tanggal_cuti', '10')
            ->where('periode', $tahun)
            ->get();
            $jumlah10=count($data_cuti10);
            $data_cuti11=Cuti::whereMonth('tanggal_cuti', '11')
            ->where('periode', $tahun)
            ->get();
            $jumlah11=count($data_cuti11);
            $data_cuti12=Cuti::whereMonth('tanggal_cuti', '12')
            ->where('periode', $tahun)
            ->get();
            $jumlah12=count($data_cuti12);

        return view('index',compact('karyawan','cuti','hrd','departemen','jumlah1','jumlah2','jumlah3','jumlah4','jumlah5','jumlah6','jumlah7','jumlah8','jumlah9','jumlah10','jumlah11','jumlah12','tahun'));
    }
    public function index_karyawan()
    {
        $id=Auth::guard('karyawans')->user()->id;
        $employe=Karyawan::find($id);
        return view('/karyawan/index',compact('employe'));
    }
}
