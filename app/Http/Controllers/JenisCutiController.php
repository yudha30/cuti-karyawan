<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jenis_Cuti;
class JenisCutiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jenis=Jenis_cuti::All();
        return view('/hrd/jenis_cuti',compact('jenis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'jenis_cuti' => 'required',
        ]);

        $jenis=new Jenis_Cuti;
        $jenis->jenis = ucwords($request->jenis_cuti);
        $jenis->save();
        return redirect('/jenis_cuti')->with('sukses','Berhasil Menambahkan Data!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $jenis=Jenis_Cuti::find($id)->update([
            'jenis' => ucwords($request->jenis_cuti)
        ]);
        return redirect('/jenis_cuti')->with('edit','Data Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Jenis_Cuti::destroy($id);
        return redirect('/jenis_cuti')->with('hapus','Berhasil Menghapus Data');
    }
}
