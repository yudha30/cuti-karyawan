<?php

namespace App\Http\Controllers\Karyawan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Karyawan;

class KaryawanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('karyawan.Dashboard.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function tampil_ubah_password($id)
    {
        $user=Karyawan::find($id);
        return view('karyawan.Ubah_Password.ubah_password',compact('user'));
    }

    public function ubah_password(Request $request)
    {
        $validatedData = $request->validate([
            'password_baru' => 'required|min:8',
            'konfirmasi_password_baru' => 'required|same:password_baru',
        ]);

        $employe=Karyawan::find($request->id);
        if (Hash::check($request->password_lama, $employe->password)) {
            $employe->update([
                'password'=>Hash::make($request->konfirmasi_password_baru)
            ]);
            return view('karyawan.index',compact('employe'))->with('edit','edit');
        }
        else{
            return redirect()->back()->with('gagal','gagal');
        }
    }
}
