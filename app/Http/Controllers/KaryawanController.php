<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Karyawan;
use Illuminate\Support\Facades\Hash;
use App\Jabatan;
use App\Departemen;
use File;

class KaryawanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jabatan = Jabatan::all();
        $departemen = Departemen::all();
        $karyawan = Karyawan::all();
        return view ('/hrd/karyawan',compact('karyawan','jabatan','departemen'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validatedData = $request->validate([
            'nik' => 'required|numeric|unique:karyawan,nik',
            'masuk_kerja' => 'required',
            'nama' => 'required',
            'jenis_kelamin'=>'required',
            'jabatan' => 'required',
            'departemen' => 'required',
            'alamat' => 'required',
            'notelp' => 'required',
            'password'=>'required|min:8',
            'foto'=>'image',
        ]);
        $foto="";
        $gambar;
        if($request->hasFile('foto')){
            $request->file('foto')->move('foto_karyawan/foto/',date("dmYhis").$request->file('foto')->getClientOriginalName());
            $foto=date("dmYhis").$request->file('foto')->getClientOriginalName();
        }
        $gambar=$foto;
        $data = new Karyawan;
            $data->nik = $request->nik;
            $data->masuk_kerja = $request->masuk_kerja;
            $data->name= $request->nama;
            $data->gambar= $gambar;
            $data->jenis_kelamin=$request->jenis_kelamin;
            $data->email=$request->email;
            $data->password=Hash::make($request->password);
            $data->departemen_id=$request->departemen;
            $data->jabatan_id=$request->jabatan;
            $data->alamat=$request->alamat;
            $data->no_hp=$request->notelp;
            $data->sisa_cuti=12;
        $data->save();
        return redirect('/karyawan')->with('sukses','Berhasil Menambahkan Data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $jabatan=Jabatan::all();
        $departemen=Departemen::all();
        $karyawan = Karyawan::find($id);
        return view('/hrd/detail_karyawan', compact('karyawan','jabatan','departemen'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'nik' => 'required|numeric',
            'masuk_kerja' => 'required',
            'nama' => 'required',
            'jenis_kelamin'=>'required',
            'jabatan' => 'required',
            'departemen' => 'required',
            'alamat' => 'required',
            'notelp' => 'required',
            'sisa_cuti'=>'required',
            'foto'=>'image',
        ]);
        $gambar=Karyawan::find($id);
        $masuk=Karyawan::find($id);
        if($request->hasFile('fotoEdit')){
            File::delete('foto_karyawan/foto/'.$gambar->gambar);
            $request->file('fotoEdit')->move('foto_karyawan/foto/',date("dmYhis").$request->file('fotoEdit')->getClientOriginalName());
            $masuk->gambar=date("dmYhis").$request->file('fotoEdit')->getClientOriginalName();
            $masuk->save();
        }else{
        $karyawan=Karyawan::where('id',$id)->update([
            'nik' => $request->nik,
            'masuk_kerja' => $request->masuk_kerja,
            'name' => $request->nama,
            'jenis_kelamin'=>$request->jenis_kelamin,
            'email'=>$request->email,
            'departemen_id'=>$request->departemen,
            'jabatan_id'=>$request->jabatan,
            'alamat'=>$request->alamat,
            'no_hp'=>$request->notelp,
            'sisa_cuti'=>$request->sisa_cuti
        ]);
        }
        return redirect('/karyawan')->with('edit','Berhasil Mengedit Data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $karyawan=Karyawan::Find($id);
        File::delete('foto_karyawan/foto/'.$karyawan->gambar);
        Karyawan::destroy($id);
        return redirect('/karyawan')->with('hapus','Berhasil Menghapus Data');
    }
}
