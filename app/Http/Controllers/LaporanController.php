<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cuti;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Auth;

class LaporanController extends Controller
{
    public function index()
    {
        return view('hrd.laporan');
    }

    public function filter_bulan($bulan)
    {
        $tahun = date('Y');
        $cuti = DB::table('cuti')
        ->join('jenis_cuti','cuti.jenis_cuti_id','=','jenis_cuti.id')
        ->whereMonth('tanggal_cuti', $bulan)
        ->where('periode', $tahun)
        ->select('cuti.*','jenis_cuti.jenis')
        ->get();
        $inc=0;
        foreach($cuti as $item){
            $inc++;
            echo
            '<tr>
                <td>'.$inc.'</td>
                <td>'.$item->nama.'</td>
                <td>'.$item->jenis.'</td>
                <td>'.date('d-m-Y',strtotime($item->tanggal_cuti)).'</td>
                <td>'.date('d-m-Y',strtotime($item->tanggal_masuk)).'</td>
                <td>'.$item->keperluan.'</td>
                <td>'.$item->status.'</td>
            </tr>';
        }
    }

    public function cek()
    {
        return view('/hrd/template_laporan');
    }

    public function cetak(Request $request)
    {
            $nama=Auth::user()->name;
            $bulan= $request->bulan;
            $tahun = date('Y');
            $cuti = DB::table('cuti')
            ->join('jenis_cuti','cuti.jenis_cuti_id','=','jenis_cuti.id')
            ->join('karyawan','cuti.karyawan_id','=','karyawan.id')
            ->whereMonth('tanggal_cuti', $bulan)
            ->where('periode', $tahun)
            ->select('cuti.*','jenis_cuti.jenis','karyawan.nik')
            ->get();

            $acc=DB::table('cuti')
            ->join('jenis_cuti','cuti.jenis_cuti_id','=','jenis_cuti.id')
            ->whereMonth('tanggal_cuti', $bulan )
            ->where('periode', $tahun)
            ->where('status','=','Diterima')
            ->select('cuti.*','jenis_cuti.jenis')
            ->get();

            $tolak=DB::table('cuti')
            ->join('jenis_cuti','cuti.jenis_cuti_id','=','jenis_cuti.id')
            ->whereMonth('tanggal_cuti', $bulan )
            ->where('periode', $tahun)
            ->where('status','=','Ditolak')
            ->select('cuti.*','jenis_cuti.jenis')
            ->get();

            $jumlahacc=count($acc);
            $jumlahtolak=count($tolak);
            $jumlahcuti=count($cuti);
        $dataLaporan = array(
            'jumlahacc' => $jumlahacc,
            'jumlahtolak' => $jumlahtolak,
            'jumlahcuti' => $jumlahcuti,
            'nama' => $nama,
            'bulan' => $bulan,
            'tahun' => $tahun,
            'cuti' => $cuti
        );
        $pdf = \PDF::loadView('/hrd/template_laporan', $dataLaporan)->setPaper('a4','landscape');
        return $pdf->download('Laporan Pengajuan Cuti.pdf');
        return redirect('/hrd/laporan');
    }
}
