<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Departemen;
use App\Jabatan;
use App\Karyawan;

class PageController extends Controller
{
    public function showdashboard()
    {
        return view ('/index');
    }
    public function menukaryawan()
    {
        $jabatan = Jabatan::all();
        $departemen = Departemen::all();
        $karyawan = Karyawan::all();
        return view ('/hrd/karyawan',compact('karyawan','jabatan','departemen'));
    }
    public function menudepartemen()
    {
        return view ('/hrd/departemen');
    }
    public function menuuser()
    {
        return view ('/hrd/user');
    }
    public function menujabatan()
    {
        return view ('/hrd/jabatan');
    }
    public function menujenis_cuti()
    {
        return view ('/hrd/jenis_cuti');
    }
    public function menucuti()
    {

    }
    public function detail_cuti()
    {
        return view ('/hrd/detail_cuti');
    }
}
