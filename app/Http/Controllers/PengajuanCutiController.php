<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Notifications\Event;
use App\Jenis_Cuti;
use App\Mail\NotifCutiLeader;
use App\Cuti;
use App\Karyawan;
use Auth;

class PengajuanCutiController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $jenis_cuti=Jenis_Cuti::all();
        $data_cuti=Cuti::where('karyawan_id',Auth::guard('karyawans')->user()->id)->get();
        $count = 0;
        $count = count($data_cuti);
        return view('/karyawan/data_cuti',compact('data_cuti','jenis_cuti','count'));
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $jenis_cuti=Jenis_Cuti::all();
        return view('/karyawan/pengajuan_cuti', compact('jenis_cuti'));
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nik' => 'required|numeric',
            'nama' => 'required',
            'tanggal_cuti' => 'required',
            'tanggal_masuk' => 'required',
            'jumlah_cuti'=>'required|numeric',
            'jenis_cuti' => 'required',
            'keperluan' => 'required',
            'periode' => 'required',
            'status' => 'required',
        ]);

            $validasiTahun=DB::table('cuti')->orderBy('periode','desc')->limit(1)->first();
            if($validasiTahun->periode == $request->periode)
            {
                $findUser = Karyawan::find(Auth::guard('karyawans')->user()->id);
                $cuti=intval($findUser->sisa_cuti);
                $jumlah_cuti=intval($request->jumlah_cuti);
                $sisa_cuti=$cuti-$jumlah_cuti;
                if($sisa_cuti>=0)
                {
                    $pengajuan_cuti = new Cuti;
                    $pengajuan_cuti->karyawan_id = Auth::guard('karyawans')->user()->id;
                    $pengajuan_cuti->nama = $request->nama;
                    $pengajuan_cuti->tanggal_cuti= $request->tanggal_cuti;
                    $pengajuan_cuti->tanggal_masuk= $request->tanggal_masuk;
                    $pengajuan_cuti->jenis_cuti_id=$request->jenis_cuti;
                    $pengajuan_cuti->jumlah_cuti=$request->jumlah_cuti;
                    $pengajuan_cuti->keperluan=$request->keperluan;
                    $pengajuan_cuti->periode=$request->periode;
                    $pengajuan_cuti->status=$request->status;
                    $pengajuan_cuti->save();

                    // update sisa cuti
                    $karyawan=Karyawan::find(Auth::guard('karyawans')->user()->id);
                    $karyawan->sisa_cuti=$sisa_cuti;
                    $karyawan->save();

                    //notifikasi pengajuan cuti ke dahsboard hrd
                    // return redirect('/karyawan/notifikasi/pengajuan_cuti');

                    return redirect('/karyawan/notifikasi/cuti/leader/'.$pengajuan_cuti->id);

                }
                else{
                    return redirect('/karyawan/data_cuti')->with('gagal','Tidak Bisa Mengajukan Cuti');
                }
            }
            else{
                $karyawan=Karyawan::find(Auth::guard('karyawans')->user()->id);
                $karyawan->sisa_cuti=12;
                $karyawan->save();

                $findUser = Karyawan::find(Auth::guard('karyawans')->user()->id);
                $cuti=intval($findUser->sisa_cuti);
                $jumlah_cuti=intval($request->jumlah_cuti);
                $sisa_cuti=$cuti-$jumlah_cuti;
                if($sisa_cuti>=0)
                {
                    $pengajuan_cuti = new Cuti;
                    $pengajuan_cuti->karyawan_id = Auth::guard('karyawans')->user()->id;
                    $pengajuan_cuti->nama = $request->nama;
                    $pengajuan_cuti->tanggal_cuti= $request->tanggal_cuti;
                    $pengajuan_cuti->tanggal_masuk= $request->tanggal_masuk;
                    $pengajuan_cuti->jenis_cuti_id=$request->jenis_cuti;
                    $pengajuan_cuti->jumlah_cuti=$request->jumlah_cuti;
                    $pengajuan_cuti->keperluan=$request->keperluan;
                    $pengajuan_cuti->periode=$request->periode;
                    $pengajuan_cuti->status=$request->status;
                    $pengajuan_cuti->save();

                    // update sisa cuti
                    $karyawan=Karyawan::find(Auth::guard('karyawans')->user()->id);
                    $karyawan->sisa_cuti=$sisa_cuti;
                    $karyawan->save();
                    return redirect('/karyawan/notifikasi/pengajuan_cuti');
                }
                else{
                    return redirect('/karyawan/data_cuti')->with('gagal','Tidak Bisa Mengajukan Cuti');
                }
            }
        }

        /**
        * Display the specified resource.
        *
        * @param  int  $id
        * @return \Illuminate\Http\Response
        */
        public function show($id)
        {
            $jenis_cuti=Jenis_Cuti::all();
            $cuti=Cuti::find($id);
            return view('/karyawan/detail_cuti', compact('cuti','jenis_cuti'));
        }

        /**
        * Show the form for editing the specified resource.
        *
        * @param  int  $id
        * @return \Illuminate\Http\Response
        */
        public function edit($id)
        {
            //
        }

        /**
        * Update the specified resource in storage.
        *
        * @param  \Illuminate\Http\Request  $request
        * @param  int  $id
        * @return \Illuminate\Http\Response
        */
        public function update(Request $request, $id)
        {
            //
        }

        /**
        * Remove the specified resource from storage.
        *
        * @param  int  $id
        * @return \Illuminate\Http\Response
        */
        public function destroy($id)
        {
            //
        }

        public function selisih(Request $request)
        {
            $date1=date_create($request->cuti);
            $date2=date_create($request->masuk);
            $difference=date_diff($date1,$date2);
            return $difference->d;
        }

        public function notif_pengajuan()
        {
            $text='Ada Pengajuan Cuti Baru. Silahkan Cek Cuti!';
            event(new Event($text));
            // return redirect('/karyawan/data_cuti')->with('sukses','Pengajuan Cuti Berhasil');
            return view('notif.popupsuccess');
        }

        public function notif_leader($id_cuti)
        {
            $user = Karyawan::find(Auth::guard('karyawans')->user()->id);

            $findLeader=DB::table('karyawan')
            ->join('departemen','karyawan.departemen_id','=','departemen.id')
            ->join('jabatan','karyawan.jabatan_id','=','jabatan.id')
            ->where('karyawan.departemen_id',$user->departemen_id)
            ->where('jabatan.jabatan','LEADER')
            ->select('karyawan.email')
            ->first();

            $cuti = DB::table('cuti')->latest()->first();

            $data=array(
                'emailLeader'=> $findLeader->email,
                'id_cuti'=>$id_cuti,
                'nama'=>$user->name,
                'departemen'=>$user->departemen->departemen,
                'tanggal_cuti' => $cuti->tanggal_cuti,
                'tanggal_masuk' => $cuti->tanggal_masuk,
                'lama_cuti'=> $cuti->jumlah_cuti,
                'keperluan' => $cuti->keperluan,
                'periode' => $cuti->periode
            );

            // kirim notifikasi email ke leader
            Mail::to($findLeader->email)->send(new NotifCutiLeader($data));

            return redirect('/karyawan/data_cuti')->with('sukses', 'Data Cuti Diterima');

        }

        public function notif_supervisor()
        {
            echo "supervisor";
        }
    }
