<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use App\User;
use File;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=User::all();
        return view ('/hrd/user',compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'email'=>'email',
            'name'=>'required',
            'password'=>'required|min:8',
            'gambar'=>'image',
        ]);

        $foto="";
        $gambar="";
        if($request->hasFile('gambar')){
            $request->file('gambar')->move('foto_user/',date("dmYhis").$request->file('gambar')->getClientOriginalName());
            $foto=date("dmYhis").$request->file('gambar')->getClientOriginalName();
        }
        $gambar=$foto;
        $data = new User;
            $data->email = $request->email;
            $data->name = $request->name;
            $data->password=Hash::make($request->password);
            $data->gambar= $gambar;
        $data->save();
        return redirect('/user')->with('sukses','Berhasil Menambahkan Data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'email'=>'email',
            'name'=>'required',
            'gambar'=>'image',
        ]);
        $gambar=User::find($id);
        $masuk=User::find($id);
        if($request->hasFile('fotoEdit')){
            File::delete('foto_user/'.$gambar->gambar);
            $request->file('fotoEdit')->move('foto_user/',date("dmYhis").$request->file('fotoEdit')->getClientOriginalName());
            $masuk->gambar=date("dmYhis").$request->file('fotoEdit')->getClientOriginalName();
            $masuk->save();
        }else{
        $karyawan=User::where('id',$id)->update([
            'email' => $request->email,
            'name' => $request->name
        ]);
        }
        return redirect('/user')->with('edit','Berhasil Mengedit Data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user=User::Find($id);
        File::delete('foto_user/'.$user->gambar);
        User::destroy($id);
        return redirect('/user')->with('hapus','Berhasil Menghapus Data');
    }

    public function show_ubah_password($id)
    {
        $user=User::Find($id);
        return view('/hrd/ubah_password',compact('user'));
    }
}
