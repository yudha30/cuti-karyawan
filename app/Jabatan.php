<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Karyawan;

class Jabatan extends Model
{
    //
    protected $table = 'jabatan';
    protected $fillable = [
        'jabatan'
    ];

    public function karyawan()
    {
        return $this->hashOne(Karyawan::class);
    }
}
