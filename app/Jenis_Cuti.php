<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Cuti;

class Jenis_Cuti extends Model
{
    //
    protected $table = 'jenis_cuti';
    protected $fillable = [
        'jenis'
    ];

    public function cuti()
    {
        return $this->hasMany(Cuti::class);
    }
}
