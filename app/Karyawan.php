<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Departemen;
use App\Jabatan;
use App\Cuti;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Karyawan extends Authenticatable
{
    //
    protected $table ='karyawan';
    protected $guard='karyawans';
    protected $fillable = [
        'nik',
        'masuk_kerja',
        'nama',
        'jenis_kelamin',
        'email',
        'password',
        'id_departemen',
        'id_jabatan',
        'alamat',
        'no_hp',
        'sisa_cuti',
    ];

    public function departemen()
    {
        return $this->belongsTo(Departemen::class);
    }

    public function jabatan()
    {
        return $this->belongsTo(Jabatan::class);
    }

    public function cuti()
    {
        return $this->hasMany(Cuti::class);
    }
}
