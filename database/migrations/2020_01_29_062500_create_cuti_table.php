<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCutiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuti', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->BigInteger('jenis_cuti_id');
            $table->string('nik',20)->unique();
            $table->string('nama',50);
            $table->date('tanggal_cuti');
            $table->date('tanggal_masuk');
            $table->string('jumlah_cuti');
            $table->string('keperluan',200);
            $table->string('periode',10);
            $table->string('status',20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuti');
    }
}
