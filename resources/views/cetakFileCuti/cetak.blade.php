<?php
$bulan = date('n');
$tahun = date('Y');
$romawi = getRomawi($bulan);
function getRomawi($bln){
                switch ($bln){
                    case 1:
                        return "I";
                        break;
                    case 2:
                        return "II";
                        break;
                    case 3:
                        return "III";
                        break;
                    case 4:
                        return "IV";
                        break;
                    case 5:
                        return "V";
                        break;
                    case 6:
                        return "VI";
                        break;
                    case 7:
                        return "VII";
                        break;
                    case 8:
                        return "VIII";
                        break;
                    case 9:
                        return "IX";
                        break;
                    case 10:
                        return "X";
                        break;
                    case 11:
                        return "XI";
                        break;
                    case 12:
                        return "XII";
                        break;
                }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cetak Cuti</title>
    <style>
        #logo{
            display: flex;
            float: left;
            width: 10%;
        }
        #logo img{
            width: 92px;
        }
        #kop{
            width: 90%;
            float: left;
            text-align: center !important;
        }
        #namaPT{
            font-size: 18px;
            font-weight: bold;
        }
        #keterangan{
            margin-top: 2% !important;
            padding-left: 4px !important;
        }
        #garis{
            margin-top: 7% !important;
            display: block;
        }
        hr{
            border-style: solid;
        }
        #detail_tabel{
            margin-left: 30% !important;
        }
        #tanggal{
            text-align: right;
            height: 150px;
        }
        #disposisi{
            padding-top: 10% !important;
        }
        #rahasia{
            font-size: 10px;
            text-align: right;
            padding-top: 42%;
        }
    </style>
</head>
<body>
    {{-- <div id="logo"><img src="images/logo kasen.png"></div> --}}
    <div id="logo"><img src="images/logo kasen.png"></div>
    <div id="kop">
            <span id="namaPT">PT KASEN INDONESIA</span>
        <br>
        EJIP Industrial Park Plot 8K-1 Sukaresmi Cikarang Selatan Bekasi, Indonesia. <br>
        Telp: 021-897-0445, Email: kasen.idn@gmail.com
    </div>
    <br>
    <div id="garis">
        <hr>
    </div>
    <div id="keterangan">Pemberitahuan Mutasi Karyawan </div>
    <table border="0">
        <tr>
            <td>NO</td>
            <td>:</td>
        <td>{{$findCuti->id}}/CUTI/{{$romawi}}/{{$tahun}}</td>
        </tr>
        <tr>
            <td>Dari</td>
            <td>:</td>
            <td>{{$findCuti->karyawan->departemen->departemen}}</td>
        </tr>
    </table>
    <table border="0" id="detail_tabel">
        <tr>
            <td>Departemen</td>
            <td>:</td>
            <td>{{$findCuti->karyawan->departemen->departemen}}</td>
        </tr>
        <tr>
            <td>Atas Nama</td>
            <td>:</td>
            <td>{{$findCuti->karyawan->name}}</td>
        </tr>
        <tr>
            <td>Terjadi Mutasi</td>
            <td>:</td>
            <td>CUTI</td>
        </tr>
        <tr>
            <td>Pada Tanggal</td>
            <td>:</td>
            <td>
                <?php
                    function tanggal_cuti($tanggal, $cetak_hari = false)
                    {
                        $bulan = array (1 =>   'Januari',
                                    'Februari',
                                    'Maret',
                                    'April',
                                    'Mei',
                                    'Juni',
                                    'Juli',
                                    'Agustus',
                                    'September',
                                    'Oktober',
                                    'November',
                                    'Desember'
                                );
                        $split 	  = explode('-', $tanggal);
                        $tgl_indo = $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];

                        if ($cetak_hari) {
                            $num = date('N', strtotime($tanggal));
                            return $hari[$num] . ', ' . $tgl_indo;
                        }
                        return $tgl_indo;
                    }
                    echo tanggal_cuti ($findCuti->tanggal_cuti);
                ?> s/d
                <?php
                    function tanggal_masuk($tanggal, $cetak_hari = false)
                    {
                        $bulan = array (1 =>   'Januari',
                                    'Februari',
                                    'Maret',
                                    'April',
                                    'Mei',
                                    'Juni',
                                    'Juli',
                                    'Agustus',
                                    'September',
                                    'Oktober',
                                    'November',
                                    'Desember'
                                );
                        $split 	  = explode('-', $tanggal);
                        $tgl_indo = $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];

                        if ($cetak_hari) {
                            $num = date('N', strtotime($tanggal));
                            return $hari[$num] . ', ' . $tgl_indo;
                        }
                        return $tgl_indo;
                    }
                    echo tanggal_masuk ($findCuti->tanggal_masuk);
                ?>
            </td>
        </tr>
    </table>

    <div id="tanggal">
        Bekasi, <?php
        function tanggal($tanggal, $cetak_hari = false)
        {
            $bulan = array (1 =>   'Januari',
                        'Februari',
                        'Maret',
                        'April',
                        'Mei',
                        'Juni',
                        'Juli',
                        'Agustus',
                        'September',
                        'Oktober',
                        'November',
                        'Desember'
                    );
            $split 	  = explode('-', $tanggal);
            $tgl_indo = $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];

            if ($cetak_hari) {
                $num = date('N', strtotime($tanggal));
                return $hari[$num] . ', ' . $tgl_indo;
            }
            return $tgl_indo;
        }
        $tanggalCreate=date('Y-m-d',strtotime($findCuti->created_at));
        echo tanggal($tanggalCreate);
    ?>
        <br><br><br>
        (..................................)
    </div>
        DISPOSISI/APPROVAL <br><br><br><br><br>
        (............................................)
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        (............................................)
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        (............................................)
    <div id="rahasia">
        #{{$findCuti->kode_rahasia}}
    </div>
</body>
</html>
