@extends('main')
@section('title','Data Cuti Karyawan')
@section('container')


<div class="content">
    <!-- Animated -->
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="col-lg-12">
                    <div class="progress">
                        <div class="progress-bar bg-success" role="progressbar" style="width: 100%" aria-valuenow="100"
                            aria-valuemin="0" aria-valuemax="100">
                        </div>
                    </div>
                    <div class="card mt-n1">
                        <div class="card-body">
                            <h4 class="box-title mb-1">Data Cuti Karyawan</h4>
                            @if(session('sukses'))
                            <script>
                                swal("Berhasil!", "Status Cuti Berhasil Diubah!", "success");
                            </script>
                            @elseif(session('gagal'))
                            <script>
                                swal("Gagal!", "Status Cuti Gagal Diubah!", "error");
                            </script>
                            @endif
                            <?php $cek=0; ?>
                            @foreach ($errors->all() as $error)
                            <?php
                            $cek++;
                            ?>
                            @endforeach
                            @if($cek>0)

                            <div class="alert alert-danger col-6" role="alert">
                                Terjadi kesalahan dalam memasukkan data. Mohon cek kembali!.
                            </div>
                            <?php $cek=0; ?>
                            @endif
                            <form action="/search/cuti" method="post">
                                <div class="row d-flex justify-content-end mb-3">
                                    @csrf
                                    <div class="col-lg-5 d-flex">
                                            <input type="text" class="form-control form-control-sm mr-2" id="search"
                                        name="search" placeholder="Cari Data Cuti"><button type="submit"
                                                class="btn btn-primary btn-sm">Cari</button>
                                    </div>
                                </div>
                            </form>
                            <div class="row">
                                <div class="col">
                                    <table class="table">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th scope="col">No</th>
                                                <th scope="col">Nama</th>
                                                <th scope="col">Tanggal Cuti</th>
                                                <th scope="col">Tanggal Masuk</th>
                                                <th scope="col">Status</th>
                                                <th scope="col">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if($count == 0)
                                            <tr>
                                            <td colspan="6" align="center">Tidak Ada Data</td>
                                            </tr>
                                            @endif
                                            @foreach ($data_cuti as $item)
                                            <tr>
                                                <th scope="row">{{$loop->iteration}}</th>
                                                <td>{{$item->nama}}</td>
                                                <td>
                                                    {{date("d-m-Y",strtotime($item->tanggal_cuti))}}</td>
                                                <td>{{date("d-m-Y",strtotime($item->tanggal_masuk))}}</td>
                                                <td>{{$item->status}}</td>
                                                <td>
                                                    @if($item->status == "Diterima" || $item->status == "Ditolak")
                                                    <a href="/hrd/detail_cuti/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                                                    @else
                                                    <a href="/hrd/detail_cuti/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                                                    <button type="submit" class="btn btn-success btn-sm" data-toggle="modal"
                                                data-target="#diterima{{$item->id}}" data-id="{{$item->id}}">Diterima</button>
                                                    <button type="submit" class="btn btn-danger btn-sm" data-toggle="modal"
                                                    data-target="#hapus1" data-id="cuti1">Ditolak</button>
                                                    @endif
                                                </td>

                                                {{-- modal diterima --}}
                                                <div class="modal fade" id="diterima{{$item->id}}" tabindex="-1" role="dialog"
                                                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">Terima
                                                                    Data Cuti
                                                                </h5>
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>Apakah Anda yakin ingin menerima data cuti karyawan
                                                                ini?</p>
                                                            </div>
                                                            <div class="modal-footer centered">
                                                            <form action="/terima/cuti/{{$item->id}}" method="post" class="ulclass">
                                                                    @method('patch')
                                                                    @csrf
                                                                    <button type="button" class="btn btn-secondary"
                                                                        data-dismiss="modal">Tutup</button>
                                                                    <button
                                                                        class="btn btn-success btn-mg">Ya</i></button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                {{-- modal ditolak --}}
                                                <div class="modal fade" id="hapus1" tabindex="-1" role="dialog"
                                                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">Tolak
                                                                    Data Cuti
                                                                </h5>
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>Apakah Anda yakin ingin menolak data cuti karyawan
                                                                    ini?</p>
                                                            </div>
                                                            <div class="modal-footer centered">
                                                            <form action="/tolak/cuti/{{$item->id}}" method="post" class="ulclass">
                                                                    @method('delete')
                                                                    @csrf
                                                                    <button type="button" class="btn btn-secondary"
                                                                        data-dismiss="modal">Tutup</button>
                                                                    <button
                                                                        class="btn btn-success btn-mg">Ya</i></button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </tr>

                                            @endforeach
                                        </tbody>
                                    </table>
                                    {{ $data_cuti->links() }}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- .animated -->
    </div>
    <!-- /.content -->
</div>

@endsection
