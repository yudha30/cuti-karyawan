@extends('main')
@section('title','Detail Data Karyawan')
@section('container')


<div class="content">
    <!-- Animated -->
    <div class="animated fadeIn">

        <div class="row">
            <div class="col-lg-12">
                <div class="progress">
                    <div class="progress-bar bg-success" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                <div class="card mt-n1">
                    <div class="card-body">
                        <h4 class="box-title mb-4">Detail Data Karyawan</h4>
                        <div class="row">
                            <div class="col-md-3">
                                <img src="/foto_karyawan/foto/{{$karyawan->gambar}}" alt="..." class="img-detail rounded float-left">
                            </div>
                            <div class="col-md-9">
                                <table class="table table-striped mb-3">
                                    <tbody>
                                        <tr>
                                            <td width="30%">NIK</td>
                                            <td width="2%">:</td>
                                            <td>{{$karyawan->nik}}</td>
                                        </tr>
                                        <tr>
                                            <td>Nama</td>
                                            <td>:</td>
                                            <td>{{$karyawan->name}}</td>
                                        </tr>
                                        <tr>
                                            <td>Email</td>
                                            <td>:</td>
                                            <td>{{$karyawan->email}}</td>
                                        </tr>
                                        <tr>
                                            <td>Jenis Kelamin</td>
                                            <td>:</td>
                                            <td>{{$karyawan->jenis_kelamin}}</td>
                                        </tr>
                                        <tr>
                                            <td>Masuk Kerja</td>
                                            <td>:</td>
                                            <td>{{$karyawan->masuk_kerja}}</td>
                                        </tr>
                                        <tr>
                                            <td>Jabatan</td>
                                            <td>:</td>
                                            <td>{{$karyawan->jabatan->jabatan}}</td>
                                        </tr>
                                        <tr>
                                            <td>Departemen</td>
                                            <td>:</td>
                                            <td>{{$karyawan->departemen->departemen}}</td>
                                        </tr>
                                        <tr>
                                            <td>Alamat</td>
                                            <td>:</td>
                                            <td>{{$karyawan->alamat}}</td>
                                        </tr>
                                        <tr>
                                            <td>No. Handphone</td>
                                            <td>:</td>
                                            <td>{{$karyawan->no_hp}}</td>
                                        </tr>
                                        <tr>
                                            <td>Sisa Cuti</td>
                                            <td>:</td>
                                            <td>{{$karyawan->sisa_cuti}} Hari</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#editModal{{$karyawan->id}}" data-id="{{$karyawan->id}}">Edit</button>
                                <!-- Modal Edit -->
                                <div class="modal fade" id="editModal{{$karyawan->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Edit Data Karyawan
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </h5>
                                            </div>
                                            <div class="modal-body">
                                                <form action="/karyawan/{{$karyawan->id}}" method="post" enctype="multipart/form-data">
                                                    @csrf
                                                    @method('patch')
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">NIK</label>
                                                        <input type="number" name="nik" class="form-control @error('nik') is-invalid @enderror" id="nik" aria-describedby="emailHelp" placeholder="Enter NIK" required value="{{$karyawan->nik}}">
                                                        @error('nik')
                                                        <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputPassword1">Masuk Kerja</label>
                                                        <input type="date" name="masuk_kerja" class="form-control @error('masuk_kerja') is-invalid @enderror" id="masuk_kerja" required value="{{$karyawan->masuk_kerja}}">
                                                        @error('masuk_kerja')
                                                        <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Nama</label>
                                                        <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror" id="nama" aria-describedby="emailHelp" placeholder="Enter Nama" required value="{{$karyawan->name}}">
                                                        @error('nama')
                                                        <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Jenis Kelamin</label>
                                                        <select class="form-control @error('jenis_kelamin') is-invalid @enderror" name="jenis_kelamin" id="jenis_kelamin" required value="{{$karyawan->jenis_kelamin}}">
                                                            <option value="Laki-Laki">Laki-Laki</option>
                                                            <option value="Perempuan">Perempuan</option>
                                                        </select>
                                                        @error('jenis_kelamin')
                                                        <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Email</label>
                                                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="email" aria-describedby="emailHelp" placeholder="Enter Email" required value="{{$karyawan->email}}">
                                                        @error('email')
                                                        <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Jabatan</label>
                                                        <select class="custom-select @error('jabatan') is-invalid @enderror" name="jabatan" id="jabatan" value="{{$karyawan->jabatan->jabatan}}">
                                                            <option value="0" selected>- Pilih Jabatan -</option>
                                                            @foreach($jabatan as $b)
                                                            @if($karyawan->jabatan->jabatan==$b->jabatan)
                                                            <option value="{{$b->id}}" selected>{{$b->jabatan}}</option>
                                                            @else
                                                            <option value="{{$b->id}}">{{$b->jabatan}}</option>
                                                            @endif
                                                            @endforeach
                                                        </select>
                                                        @error('jabatan')
                                                        <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Departemen</label>
                                                        <select class="custom-select @error('departemen') is-invalid @enderror" name="departemen" id="departemen" value="{{$karyawan->departemen->departemen}}">
                                                            <option value="0" selected>- Pilih Departemen -</option>
                                                            @foreach($departemen as $b)
                                                            @if($karyawan->departemen->departemen==$b->departemen)
                                                            <option value="{{$b->id}}" selected>{{$b->departemen}}</option>
                                                            @else
                                                            <option value="{{$b->id}}">{{$b->departemen}}</option>
                                                            @endif
                                                            @endforeach
                                                        </select>
                                                        @error('departemen')
                                                        <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Alamat</label>
                                                        <input type="text" name="alamat" class="form-control @error('alamat') is-invalid @enderror" id="alamat" aria-describedby="emailHelp" placeholder="Enter alamat" required value="{{$karyawan->alamat}}">
                                                        @error('alamat')
                                                        <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">No. Telp</label>
                                                        <input type="text" name="notelp" class="form-control @error('notelp') is-invalid @enderror" id="notelp" aria-describedby="emailHelp" placeholder="Enter notelp" required value="{{$karyawan->no_hp}}">
                                                        @error('notelp')
                                                        <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Sisa Cuti</label>
                                                        <input type="text" name="sisa_cuti" class="form-control @error('sisa_cuti') is-invalid @enderror" id="sisa_cuti" aria-describedby="emailHelp" placeholder="Enter sisa_cuti" required value="{{$karyawan->sisa_cuti}}">
                                                        @error('sisa_cuti')
                                                        <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Foto</label>
                                                        <input type="file" name="fotoEdit" class="form-control-file @error('fotoEdit') is-invalid @enderror" id="fotoEdit">
                                                        <img id="previewEdit" src="{{url('/foto_karyawan/foto/'.$karyawan->gambar)}}" width="300px">
                                                        @error('fotoEdit')
                                                            <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#hapus1" data-id="cuti1">Hapus</a>
                            </div>
                            <div class="modal fade" id="hapus1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Hapus Data Karyawan</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <p>Apakah Anda yakin ingin menghapus data karyawan ini?</p>
                                        </div>
                                        <div class="modal-footer centered">
                                            <form action="" method="post" class="ulclass">
                                                @method('delete')
                                                @csrf
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button class="btn btn-success btn-mg">Ya</i></button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
    </div>
    <!-- .animated -->
</div>
<!-- /.content -->
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function(){
        // ====== Preview Foto Edit data Karyawan ========

        function readURLEdit(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#previewEdit').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#fotoEdit").change(function () {
            readURLEdit(this);
        });
    });

</script>
@endsection

