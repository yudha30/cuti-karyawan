@extends('main')
@section('title','Data Jenis Cuti')
@section('container')


<div class="content">
    <!-- Animated -->
    <div class="animated fadeIn">

        <div class="row">
            <div class="col-lg-12">
                <div class="col-lg-12">
                    <div class="progress">
                        <div class="progress-bar bg-success" role="progressbar" style="width: 100%" aria-valuenow="100"
                            aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <div class="card mt-n1">
                        <div class="card-body">
                            <h4 class="box-title mb-4">Data Jenis Cuti</h4>
                            <a class="btn btn-success mb-3 text-light" data-toggle="modal" data-target="#tambah"><i
                                    class="fa fa-plus"> Tambah</i></a>
                            @if(session('sukses'))
                            <script>
                                swal("Berhasil!", "Data Berhasil Ditambahkan!", "success");
                            </script>
                            @elseif(session('edit'))
                            <script>
                                swal("Berhasil!", "Data Berhasil Diedit!", "success");
                            </script>
                            @elseif(session('hapus'))
                            <script>
                                swal("Berhasil!", "Data Berhasil Dihapus!", "success");
                            </script>
                            @endif
                            <?php $cek=0; ?>
                            @foreach ($errors->all() as $error)
                            <?php
                            $cek++;
                            ?>
                            @endforeach
                            @if($cek>0)

                            <div class="alert alert-danger col-6" role="alert">
                                Terjadi kesalahan dalam memasukkan data. Mohon cek kembali!.
                            </div>
                            <?php $cek=0; ?>
                            @endif
                            <table class="table">
                                <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Jenis Cuti</th>
                                        <th scope="col">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $id=0 ?>
                                    @foreach ($jenis as $item)
                                    <?php $id = $item->id   ?>
                                    <tr>
                                        <th scope="row">{{$loop->iteration}}</th>
                                    <td>{{$item->jenis}}</td>
                                        <td>
                                            <button type="button" class="btn btn-warning btn-sm" data-toggle="modal"
                                                data-target="#editModal{{$item->id}}"
                                                data-id="{{$item->id}}">Edit</button>
                                            <div class="modal fade" id="editModal{{$item->id}}" tabindex="-1"
                                                role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Edit Data
                                                                Jenis Cuti</h5>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form action="/jenis_cuti/{{$item->id}}" method="POST">
                                                                @csrf
                                                                @method('patch')
                                                                <div class="form-group">
                                                                    <label for="exampleInputEmail1">Jenis Cuti</label>
                                                                    <input type="text" name="jenis_cuti"
                                                                        class="form-control @error('jenis_cuti')
                                                                        is-invalid @enderror" id="jenis_cuti"
                                                                        aria-describedby="emailHelp"
                                                                        placeholder="Enter Jenis Cuti" required
                                                                        value="{{$item->jenis}}">
                                                                    @error('jenis_cuti')
                                                                    <span class="text-danger">{{$message}}</span>
                                                                    @enderror
                                                                </div>

                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary"
                                                                        data-dismiss="modal">Batal</button>
                                                                    <button type="submit"
                                                                        class="btn btn-primary">Simpan</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="#" class="btn btn-danger btn-sm" data-toggle="modal"
                                                data-target="#hapus1" data-id="cuti1">Hapus</a>
                                                <div class="modal fade" id="hapus1" tabindex="-1" role="dialog"
                                                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">Hapus Data Jenis
                                                                    Cuti</h5>
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                            <p>Apakah Anda yakin ingin menghapus data jenis cuti ini?</p>
                                                            </div>
                                                            <div class="modal-footer centered">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-dismiss="modal">Close</button>
                                                                <form action="/jenis_cuti/{{$item->id}}" method="post"
                                                                    class="d-inline">
                                                                    @method('delete')
                                                                    @csrf
                                                                    <button type="submit" class="btn btn-success">Hapus</button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- .animated -->
    </div>
    <!-- /.content -->
</div>

<!-- Modal Tambah -->
<div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Data Jenis Cuti</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/jenis_cuti/tambah" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1">Jenis Cuti</label>
                        <input type="text" name="jenis_cuti" class="form-control" @error('jenis_cuti') is-invalid
                            @enderror" id="jenis_cuti" aria-describedby="emailHelp" placeholder="Enter Jenis Cuti"
                            required>
                        {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> --}}
                        @error('jenis_cuti')
                        <span class="text-danger">{{$message}}</span>
                        @enderror
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{{-- akhir modal tambah --}}
<div class="clearfix"></div>

@endsection
