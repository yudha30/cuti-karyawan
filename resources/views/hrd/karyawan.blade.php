@extends('main')
@section('title','Data Karyawan')
@section('container')


<div class="content">
    <!-- Animated -->
    <div class="animated fadeIn">

        <div class="row">
            <div class="col-lg-12">
                <div class="col-lg-12">
                    <div class="progress">
                        <div class="progress-bar bg-success" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <div class="card mt-n1">
                        <div class="card-body">
                            <h4 class="box-title mb-4">Data Karyawan</h4>
                            <a class="btn btn-success mb-3 text-light" data-toggle="modal" data-target="#tambah"><i
                                class="fa fa-plus"> Tambah</i></a>
                                @if(session('sukses'))
                                <script>
                                    swal("Berhasil!", "Data Berhasil Ditambahkan!", "success");
                                </script>
                                @elseif(session('edit'))
                                <script>
                                    swal("Berhasil!", "Data Berhasil Diubah!", "success");
                                </script>
                                @elseif(session('hapus'))
                                <script>
                                    swal("Berhasil!", "Data Berhasil Dihapus!", "success");
                                </script>
                                @endif
                                <?php $cek=0; ?>
                                @foreach ($errors->all() as $error)
                                <?php
                                $cek++;
                                ?>
                                @endforeach
                                @if($cek>0)

                                <div class="alert alert-danger col-6" role="alert">
                                    Terjadi kesalahan dalam memasukkan data. Mohon cek kembali!.
                                </div>
                                <?php $cek=0; ?>
                                @endif
                                <table class="table">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Nama</th>
                                            <th scope="col">Email</th>
                                            <th scope="col">Jabatan</th>
                                            <th scope="col">Departemen</th>
                                            <th scope="col">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($karyawan as $item)
                                        <tr>
                                            <th scope="row">{{$loop->iteration}}</th>
                                            <td>{{$item->name}}</td>
                                            <td>{{$item->email}}</td>
                                            <td>{{$item->jabatan->jabatan}}</td>
                                            <td>{{$item->departemen->departemen}}</td>
                                            <td>
                                                <a href="/detail_karyawan/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                                                <a href="#" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#hapus{{$item->id}}" data-id="cuti1">Hapus</a>
                                                <div class="modal fade" id="hapus{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">Hapus Data Karyawan
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button></h5>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <p>Apakah Anda yakin ingin menghapus data karyawan ini?</p>
                                                                </div>
                                                                <div class="modal-footer centered">
                                                                    @method('delete')
                                                                    @csrf
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                    <form action="/karyawan/{{$item->id}}" method="post" class="d-inline">
                                                                        @method('delete')
                                                                        @csrf
                                                                        <button type="submit" class="btn btn-success">Ya</button>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Modal Tambah -->
                    <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Tambah Data Karyawan
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button></h5>
                                </div>
                                <div class="modal-body">
                                    <form action="/karyawan/tambah" method="post" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">NIK</label>
                                            <input type="number" name="nik" class="form-control @error('nik') is-invalid @enderror"
                                            id="nik" aria-describedby="emailHelp" placeholder="Enter NIK" required
                                            value="{{old('nik')}}">
                                            @error('nik')
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Masuk Kerja</label>
                                            <input type="date" name="masuk_kerja"
                                            class="form-control @error('masuk_kerja') is-invalid @enderror" id="masuk_kerja"
                                            required value="{{old('masuk_kerja')}}">
                                            @error('masuk_kerja')
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Nama</label>
                                            <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror"
                                            id="nama" aria-describedby="emailHelp" placeholder="Enter Nama" required
                                            value="{{old('nama')}}">
                                            @error('nama')
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Jenis Kelamin</label>
                                            <select class="form-control @error('jenis_kelamin') is-invalid @enderror"
                                            name="jenis_kelamin" id="jenis_kelamin" required value="{{old('masuk_kerja')}}">
                                            <option value="Laki-Laki">Laki-Laki</option>
                                            <option value="Perempuan">Perempuan</option>
                                        </select>
                                        @error('jenis_kelamin')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Email</label>
                                        <input type="email" name="email"
                                        class="form-control @error('email') is-invalid @enderror" id="email"
                                        aria-describedby="emailHelp" placeholder="Enter Email" required
                                        value="{{old('email')}}">
                                        @error('email')
                                        <span class="text-danger">{{$message}}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Jabatan</label>
                                        <select class="custom-select @error('jabatan') is-invalid @enderror" name="jabatan"
                                        id="jabatan" value="{{old('jabatan')}}">
                                        <option value="0" selected>- Pilih Jabatan -</option>
                                        @foreach($jabatan as $b)
                                        @if(old('jabatan')==$b->id)
                                        <option value="{{$b->id}}" selected>{{$b->jabatan}}</option>
                                        @else
                                        <option value="{{$b->id}}">{{$b->jabatan}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                    @error('jabatan')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Departemen</label>
                                    <select class="custom-select @error('departemen') is-invalid @enderror"
                                    name="departemen" id="departemen" value="{{old('departemen')}}" required="">
                                    <option value="" selected>- Pilih Departemen -</option>
                                    @foreach($departemen as $b)
                                    @if(old('departemen')==$b->id)
                                    <option value="{{$b->id}}" selected>{{$b->departemen}}</option>
                                    @else
                                    <option value="{{$b->id}}">{{$b->departemen}}</option>
                                    @endif
                                    @endforeach
                                </select>
                                @error('departemen')
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Alamat</label>
                                <input type="text" name="alamat"
                                class="form-control @error('alamat') is-invalid @enderror" id="alamat"
                                aria-describedby="emailHelp" placeholder="Enter alamat" required
                                value="{{old('alamat')}}">
                                @error('alamat')
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">No. Telp</label>
                                <input type="text" name="notelp"
                                class="form-control @error('notelp') is-invalid @enderror" id="notelp"
                                aria-describedby="emailHelp" placeholder="Enter notelp" required
                                value="{{old('notelp')}}">
                                @error('notelp')
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Password</label>
                                <input type="password" name="password"
                                class="form-control @error('password') is-invalid @enderror" id="password"
                                aria-describedby="emailHelp" placeholder="Enter password" required>
                                @error('password')
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Foto</label>
                                <input type="file" name="foto"
                                class="form-control-file @error('foto') is-invalid @enderror" id="foto" required>
                                <img id="preview" src="" width="300px">
                                @error('foto')
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
</div>
<!-- .animated -->
</div>
<!-- /.content -->
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function(){
        // ====== Preview Foto tambah data Karyawan ========
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#foto").change(function () {
            readURL(this);
        });


        // ====== Preview Foto Edit data Karyawan ========

        function readURLEdit(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#previewEdit').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#fotoEdit").change(function () {
            readURLEdit(this);
        });
    });

</script>
@endsection
