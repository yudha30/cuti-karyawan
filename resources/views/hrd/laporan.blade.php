@extends('main')
@section('title','Laporan')
@section('container')


<div class="content">
    <!-- Animated -->
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="col-lg-12">
                    <div class="progress n-1">
                        <div class="progress-bar bg-success" role="progressbar" style="width: 100%" aria-valuenow="100"
                            aria-valuemin="0" aria-valuemax="100">
                        </div>
                    </div>
                    <div class="card mt-n1 d-flex">
                        <div class="card-body">
                            <h4 class="box-title mb-4">Laporan Cuti Pegawai</h4>
                            <div class="row ">
                                <div class="col-3 d-flex d-inline-block">
                                    Bulan
                                    <form action="cetak_laporan" method="POST">
                                    @csrf
                                        <select class="form-control form-control-sm ml-1" name="bulan" id="bulan">
                                            <option value="" hidden>- Pilih Bulan -</option>
                                            <option value="01">Januari</option>
                                            <option value="02">Februari</option>
                                            <option value="03">Maret</option>
                                            <option value="04">April</option>
                                            <option value="05">Mei</option>
                                            <option value="06">Juni</option>
                                            <option value="07">Juli</option>
                                            <option value="08">Agustus</option>
                                            <option value="09">September</option>
                                            <option value="10">Oktober</option>
                                            <option value="11">November</option>
                                            <option value="12">Desember</option>
                                        </select>
                                    </div>
                                    <div class="col-9 mb-3 text-right">
                                        <button type="submit" id="cetak" class="btn btn-success btn-sm pl-3 pr-3">Cetak&nbsp; <i class="fa fa-print"></i></button>
                                    </div>
                                </form>
                            </div>
                            @if(session('sukses'))
                            <script>
                                swal("Berhasil!", "Data Berhasil Ditambahkan!", "success");

                            </script>
                            @elseif(session('edit'))
                            <script>
                                swal("Berhasil!", "Data Berhasil Diedit!", "success");

                            </script>
                            @elseif(session('hapus'))
                            <script>
                                swal("Berhasil!", "Data Berhasil Dihapus!", "success");

                            </script>
                            @endif
                            <?php $cek=0; ?>
                            @foreach ($errors->all() as $error)
                            <?php
                            $cek++;
                            ?>
                            @endforeach
                            @if($cek>0)

                            <div class="alert alert-danger col-6" role="alert">
                                Terjadi kesalahan dalam memasukkan data. Mohon cek kembali!.
                            </div>
                            <?php $cek=0; ?>
                            @endif
                            <table class="table">
                                <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Nama</th>
                                        <th scope="col">Jenis Cuti</th>
                                        <th scope="col">Tanggal Cuti</th>
                                        <th scope="col">Tanggal Masuk</th>
                                        <th scope="col">Keperluan</th>
                                        <th scope="col">Status</th>
                                    </tr>
                                </thead>
                                <tbody id="tabelCuti">
                                    <tr>
                                        <td colspan="7" align="center">Cari Data Terlebih Dahulu</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- .animated -->
    </div>
    <!-- /.content -->
</div>
<div class="clearfix">

</div>


<script>
    $('#bulan').change(function () {
        let bulan = $('#bulan').val();
        // get token
        var CSRF_TOKEN = $('input[name="_token"]').attr('value');
        // kirim data
        jQuery.ajax({
            type: 'get',
            url: '/laporan/bulan/'+bulan,
            success: function (data) {
                console.log(data);
                if(data!=null)
                {
                    $('#tabelCuti').html(data);
                }
                if(data=='' || data==null){
                    $('#tabelCuti').html('<tr><td colspan="7" align="center">Data Tidak ditemukan</td></tr>');
                }
            }
        });
    })
</script>
<script>
    $('#cetak').change(function () {
        let bulan = $('#bulan').val();
        // get token
        var CSRF_TOKEN = $('input[name="_token"]').attr('value');
        // kirim data
        jQuery.ajax({
            type: 'get',
            url: '/cetak_laporan/'+bulan,
            success: function (data) {
                console.log(data);
                if(data=='' || data==null){
                    $('#tabelCuti').html('<tr><td colspan="7" align="center">Data Tidak ditemukan</td></tr>');
                }
            }
        });
    })
</script>
@endsection
