<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laporan</title>
    <style>
        .namapt{
            color: #0275d8;
            font-size: 25px;
            letter-spacing: 3px;
            font-weight: bold;
            text-align: center;
            text-transform: uppercase;
        }
        .subnama{
            text-transform: uppercase;
            color: #0275d8;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 18px;
            letter-spacing: 0 !important;
            font-weight: 400;
            text-align: center;
            padding: 0;
        }
        .foot{
            padding: 0;
        }
        .email{
            color:#0275d8;
        }
        
        hr{
            border: 2px solid;
            /* border-style: double; */

        }
        .judul{
            font-size: 16px;
            font-weight: 600;
            text-align: center;
        }
        .judultable{
            font-size: 15px;
            text-align: center;
            padding-top: 5px;
            padding-bottom: 5px;
            font-weight: bold;
        }
        .table-satu{
            text-align: left;
            padding: 10px 5px;
            margin:auto;
        }
        .table-dua{
            width: 700px;
            text-align: center;
            border: 0;
            margin:auto;
        }
        .table-tiga{
            width: 300px;
            text-align: left;
            border: 0;
        }
        .img-logo{
            margin-bottom: -10px;
            width: 120px;
        }
        table {
            border-collapse: collapse;
            width: 100%;
        }
        
    </style>
</head>
<body>
    <table class="table-dua">
        <tr>

            <td width="20%" align="left" rowspan="3"><img src="images/logo kasen.png" class="img-logo"></td>
        </tr>
        <tr>
            <td class="namapt">PT.KASEN INDONESIA<br></td>
        </tr>
        <tr>
            <td align="center">EJIP Industrial Park Plot 8K-1, Sukaresmi, Cikarang Selatan, Bekasi, INDONESIA<br>Telp : 021-097-0445, E-mail : <span class="email">kasen.idn@gmail.com</span></td>
        </tr>
    </table>
    <hr>
    <?php $month="" ?>
    @if($bulan==1)
    @php
    $month="Januari";
    @endphp
    
    @elseif($bulan==2)
    @php
    $month="Februari";
    @endphp
    
    @elseif($bulan==3)
    @php
    $month="Maret";
    @endphp
    
    @elseif($bulan==4)
    @php
    $month="April";
    @endphp
    
    @elseif($bulan==5)
    @php
    $month="Mei";
    @endphp
    
    @elseif($bulan==6)
    @php
    $month="Juni";
    @endphp
    
    @elseif($bulan==7)
    @php
    $month="Juli";
    @endphp
    
    @elseif($bulan==8)
    @php
    $month="Agustus";
    @endphp
    
    @elseif($bulan==9)
    @php
    $month="September";
    @endphp
    
    @elseif($bulan==10)
    @php
    $month="Oktober";
    @endphp
    
    @elseif($bulan==11)
    @php
    $month="November";
    @endphp
    
    @else
    @php
    $month="Desember";
    @endphp
    
    @endif
    
    <h3 class="judul">Laporan Pengajuan Cuti Karyawan Bulan {{$month}} Tahun {{$tahun}}</h3>
    
    
    <table width="100%" border="1" class="table-satu">
        <tr class="judultable">
            <td width=2%>No</td>
            <td width=13%>NIK</td>
            <td width=10%>Nama</td>
            <td width=12%>Tanggal Cuti</td>
            <td width=12%>Tanggal Masuk</td>
            <td width=10%>Jumlah Cuti</td>
            <td width=10%>Jenis Cuti</td>
            <td width=15%>Keperluan</td>
            <td width=8%>Periode</td>
            <td width=8%>Status</td>
        </tr>
        @foreach ($cuti as $item)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$item->nik}}</td>
            <td>{{$item->nama}}</td>
            <td>
                {{date("d-m-Y",strtotime($item->tanggal_cuti))}}
            </td>
            <td>
                {{date("d-m-Y",strtotime($item->tanggal_masuk))}}
            </td>
            <td>{{$item->jumlah_cuti}} Hari</td>
            <td>{{$item->jenis}}</td>
            <td>{{$item->keperluan}}</td>
            <td>{{$item->periode}}</td>
            <td>{{$item->status}}</td>
        </tr>
        @endforeach
    </table>
    <br>
    <table class="table-tiga">
        <tr>
            <td width="70%">Jumlah Pengajuan Cuti</td>
            <td width="2%">:</td>
            <td>{{$jumlahcuti}}</td>
        </tr>
        <tr>
            <td>Jumlah Pengajuan Diterima</td>
            <td>:</td>
            <td>{{$jumlahacc}}</td>
        </tr>
        <tr>
            <td>Jumlah Pengajuan Ditolak</td>
            <td>:</td>
            <td>{{$jumlahtolak}}</td>
        </tr>
    </table>
    <table border="0" width="100%">
        <tr>
            <td width="70%"></td>
            <td width="30%" align="center">Bekasi,
                <?php
                function tanggal_masuk($tanggal, $cetak_hari = false)
                {
                    $bulan = array (1 =>   'Januari',
                    'Februari',
                    'Maret',
                    'April',
                    'Mei',
                    'Juni',
                    'Juli',
                    'Agustus',
                    'September',
                    'Oktober',
                    'November',
                    'Desember'
                    );
                    $split 	  = explode('-', $tanggal);
                    $tgl_indo = $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
                    
                    if ($cetak_hari) {
                        $num = date('N', strtotime($tanggal));
                        return $hari[$num] . ', ' . $tgl_indo;
                    }
                    return $tgl_indo;
                }
                $tgl=date('Y-m-d');
                echo tanggal_masuk ($tgl);
                ?></td>
            </tr>
            <tr>
                <td></td>
                <td align="center">Human Resources Management</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td align="center">{{$nama}}</td>
            </tr>
        </table>
    </body>
    </html>
    