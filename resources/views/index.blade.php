@extends('main')
@section('title','Dashboard')
@section('container')


<div class="content">
    <!-- Animated -->
    <div class="animated fadeIn">
        <!-- Widgets  -->
        <div class="row">
            @if(session('sukses'))
            <div class="alert alert-success col-md-12" role="alert">
                {{session('sukses')}}
            </div>
            @elseif(session('gagal'))
            <div class="alert alert-danger col-md-12" role="alert">
                {{session('gagal')}}
            </div>
            @endif
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-five">
                            <div class="stat-icon dib flat-color-2">
                                <i class="pe-7s-browser"></i>
                            </div>
                            <div class="stat-content">
                                <div class="text-left dib">
                                    <div class="stat-text"><span class="count">{{$cuti}}</span></div>
                                    <div class="stat-heading">Total Cuti</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-five">
                            <div class="stat-icon dib flat-color-2">
                                <i class="pe-7s-user"></i>
                            </div>
                            <div class="stat-content">
                                <div class="text-left dib">
                                    <div class="stat-text"><span class="count">{{$hrd}}</span></div>
                                    <div class="stat-heading">HRD</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-five">
                            <div class="stat-icon dib flat-color-3">
                                <i class="fa fa-building"></i>
                            </div>
                            <div class="stat-content">
                                <div class="text-left dib">
                                    <div class="stat-text"><span class="count">{{$departemen}}</span></div>
                                    <div class="stat-heading">Departemen</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="stat-widget-five">
                            <div class="stat-icon dib flat-color-4">
                                <i class="pe-7s-users"></i>
                            </div>
                            <div class="stat-content">
                                <div class="text-left dib">
                                    <div class="stat-text"><span class="count">{{$karyawan}}</span></div>
                                    <div class="stat-heading">Karyawan</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Widgets -->
        <!--  Traffic  -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="box-title">Grafik </h4>
                    </div>
                    <div class="row">
                        <div class="col-lg-12" id="grafik">
                            <div class="card-body">
                                <!-- <canvas id="TrafficChart"></canvas>   -->
                                <div id="traffic-chart" class="traffic-chart">
                                    grafik
                                </div>
                            </div>
                        </div>
                    </div> <!-- /.row -->
                    <div class="card-body"></div>
                </div>
            </div><!-- /# column -->
        </div>
        <!--  /Traffic -->
        <div class="clearfix"></div>
    </div>
    <!-- .animated -->
</div>

<script src="{{ asset('js/highcharts.js') }}"></script>
<script>
    Highcharts.chart('grafik', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Grafik Data Cuti Tahun {!!json_encode($tahun)!!}'
    },

    xAxis: {
        categories: [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Aug',
            'Sep',
            'Oct',
            'Nov',
            'Dec'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Jumlah'
        },
        tickInterval: 5
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y} </b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Permohonan',
        data: [{!!json_encode($jumlah1)!!},{!!json_encode($jumlah2)!!},{!!json_encode($jumlah3)!!},{!!json_encode($jumlah4)!!},{!!json_encode($jumlah5)!!},{!!json_encode($jumlah6)!!},{!!json_encode($jumlah7)!!},{!!json_encode($jumlah8)!!},{!!json_encode($jumlah9)!!},{!!json_encode($jumlah10)!!},{!!json_encode($jumlah11)!!},{!!json_encode($jumlah12)!!}]

    }]
});
</script>
@endsection
