<!doctype html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Ubah Password</title>
    <meta name="description" content="Ela Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="https://i.imgur.com/QRAUqs9.png">
    <link rel="shortcut icon" href="https://i.imgur.com/QRAUqs9.png">

    <link rel="stylesheet" href="/assets/css/normalize.min.css">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/css/themify-icons.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.0/css/flag-icon.min.css">
    <link rel="stylesheet" href="/assets/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="/assets/css/style.css">

    <script src="{{asset('/js/sweetalert.min.js')}}"></script>

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>


</head>
<body class="bg-light">
    @if(session('gagal'))
    <script>
        swal("Gagal!", "Data Password Lama Tidak Sesuai. Silahkan Cek Kembali", "error");
    </script>
    @endif
    <div class="sufee-login d-flex align-content-center flex-wrap">
        <div class="container">
            <div class="login-content">
                <div class="login-form shadow">
                <form method="POST" action="/karyawan/ubah_password/{{Auth::user()->id}}">
                        @csrf
                            <div class="form-group">
                                @if(session('sukses'))
                            <div class="alert alert-success col-12" role="alert">
                                {{session('sukses')}}
                            </div>
                            @endif
                            <label>Email address</label>
                            <input type="email" name="email" id="email" class="form-control" readonly value={{$user->email}}>
                            <input type="hidden" name="id" id="id" class="form-control" readonly value={{$user->id}}>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Password Lama</label>
                            <input type="password" name="password_lama" id="password_lama" class="form-control" required="">
                            @error('password_lama')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Password Baru</label>
                            <input type="password" name="password_baru" id="password_baru" class="form-control @error('password_baru') is-invalid @enderror" required="">
                            @error('password_baru')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Konfirmasi Password Baru</label>
                            <input type="password" name="konfirmasi_password_baru" id="konfirmasi_password_baru" class="form-control @error('konfirmasi_password_baru') is-invalid @enderror" required="">
                            @error('konfirmasi_password_baru')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30">Ubah</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="/assets/js/jquery.min.js"></script>
    <script src="/assets/js/popper.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/jquery.matchHeight.min.js"></script>
    <script src="/assets/js/main.js"></script>

</body>
</html>
