@extends('/karyawan/header')
@section('title','Data Cuti Karyawan')
@section('container')


<div class="content">
    <!-- Animated -->
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="col-lg-12">
                    <div class="progress">
                        <div class="progress-bar bg-success" role="progressbar" style="width: 100%" aria-valuenow="100"
                            aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <div class="card mt-n1">
                        <div class="card-body table-responsive">
                            <h4 class="box-title mb-4">Data Cuti Karyawan</h4>
                            @if(session('sukses'))
                            <script>
                                swal("Berhasil!", "Berhasil Mengajukan Cuti!", "success");

                            </script>
                            @elseif(session('gagal'))
                            <script>
                                swal("Gagal!", "Gagal!", "error");

                            </script>
                            @endif
                            <?php $cek=0; ?>
                            @foreach ($errors->all() as $error)
                            <?php
                            $cek++;
                            ?>
                            @endforeach
                            @if($cek>0)

                            <div class="alert alert-danger col-6" role="alert">
                                Terjadi kesalahan dalam memasukkan data. Mohon cek kembali!.
                            </div>
                            <?php $cek=0; ?>
                            @endif
                            <table class="table">
                                <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">NIK</th>
                                        <th scope="col">Nama</th>
                                        <th scope="col">Tanggal Cuti</th>
                                        <th scope="col">Jenis Cuti</th>
                                        <th scope="col">Keperluan</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Detail</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($count != 0)
                                        @foreach ($data_cuti as $item)
                                        <tr>
                                            <th scope="row">{{$loop->iteration}}</th>
                                            <td>{{$item->karyawan->nik}}</td>
                                            <td>{{$item->nama}}</td>
                                            <td>{{date('d-m-Y',strtotime($item->tanggal_cuti))}}</td>
                                            <td>{{$item->jenis_cuti->jenis}}</td>
                                            <td>{{$item->keperluan}}</td>
                                            <td>
                                                @if($item->status=="Ditolak")
                                                <span class="btn badge-danger btn-sm">
                                                    {{$item->status}}
                                                </span>
                                                @elseif($item->status=="Pengajuan")
                                                <span class="btn badge-warning btn-sm">
                                                    {{$item->status}}
                                                </span>
                                                @elseif($item->status=="Diterima")
                                                <span class="btn badge-success btn-sm">
                                                    {{$item->status}}
                                                </span>
                                                @else
                                                <span class="btn badge-warning btn-sm">
                                                    {{$item->status}}
                                                </span>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="/detail_cuti/{{$item->id}}"
                                                    class="btn btn-outline-primary btn-sm">Detail</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    @else
                                    <tr>
                                        <td colspan="8" align="center">Tidak Ada Data Cuti</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


            <div class="clearfix"></div>
        </div>
        <!-- .animated -->
    </div>
</div>
<!-- /.content -->
@endsection

