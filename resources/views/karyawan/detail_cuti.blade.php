@extends('/karyawan/header')
@section('title','Data Cuti Karyawan')
@section('container')

<div class="content">
    <!-- Animated -->
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="col-lg-12">
                    <div class="progress">
                        <div class="progress-bar bg-success" role="progressbar" style="width: 100%" aria-valuenow="100"
                            aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <div class="card mt-n1">
                        <h4 class="box-title mb-3 ml-3">Detail Cuti Karyawan</h4>
                        <div class="col-md-9">
                            <table class="table table-striped mb-3">
                                <tbody>
                                    <tr>
                                        <td width="30%">NIK</td>
                                        <td width="2%">:</td>
                                        <td>{{$cuti->karyawan->nik}}</td>
                                    </tr>
                                    <tr>
                                        <td>Nama</td>
                                        <td>:</td>
                                        <td>{{$cuti->nama}}</td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Cuti</td>
                                        <td>:</td>
                                        <td>
                                            <?php
                                            function tanggal_cuti($tanggal, $cetak_hari = false)
                                            {
                                                $bulan = array (1 =>   'Januari',
                                                            'Februari',
                                                            'Maret',
                                                            'April',
                                                            'Mei',
                                                            'Juni',
                                                            'Juli',
                                                            'Agustus',
                                                            'September',
                                                            'Oktober',
                                                            'November',
                                                            'Desember'
                                                        );
                                                $split 	  = explode('-', $tanggal);
                                                $tgl_indo = $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];

                                                if ($cetak_hari) {
                                                    $num = date('N', strtotime($tanggal));
                                                    return $hari[$num] . ', ' . $tgl_indo;
                                                }
                                                return $tgl_indo;
                                            }
                                            echo tanggal_cuti ($cuti->tanggal_cuti);
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Masuk</td>
                                        <td>:</td>
                                        <td>
                                            <?php
                                            function tanggal_masuk($tanggal, $cetak_hari = false)
                                            {
                                                $bulan = array (1 =>   'Januari',
                                                            'Februari',
                                                            'Maret',
                                                            'April',
                                                            'Mei',
                                                            'Juni',
                                                            'Juli',
                                                            'Agustus',
                                                            'September',
                                                            'Oktober',
                                                            'November',
                                                            'Desember'
                                                        );
                                                $split 	  = explode('-', $tanggal);
                                                $tgl_indo = $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];

                                                if ($cetak_hari) {
                                                    $num = date('N', strtotime($tanggal));
                                                    return $hari[$num] . ', ' . $tgl_indo;
                                                }
                                                return $tgl_indo;
                                            }
                                            echo tanggal_masuk ($cuti->tanggal_masuk);
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Jumlah Cuti</td>
                                        <td>:</td>
                                        <td>{{$cuti->jumlah_cuti}} Hari</td>
                                    </tr>
                                    <tr>
                                        <td>Jenis Cuti</td>
                                        <td>:</td>
                                        <td>{{$cuti->jenis_cuti->jenis}}</td>
                                    </tr>
                                    <tr>
                                        <td>Keperluan</td>
                                        <td>:</td>
                                        <td>{{$cuti->keperluan}}</td>
                                    </tr>
                                    <tr>
                                        <td>Periode</td>
                                        <td>:</td>
                                        <td>{{$cuti->periode}}</td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td>:</td>
                                        <td>
                                            @if($cuti->status=="Ditolak")
                                            <span class="btn badge-danger btn-sm">
                                                {{$cuti->status}}
                                            </span>
                                            @elseif($cuti->status=="Pengajuan")
                                            <span class="btn badge-warning btn-sm">
                                                {{$cuti->status}}
                                            </span>
                                            @elseif($cuti->status=="Diterima")
                                            <span class="btn badge-success btn-sm">
                                                {{$cuti->status}}
                                            </span>
                                            @else
                                            <span class="btn badge-warning btn-sm">
                                                {{$cuti->status}}
                                            </span>
                                            @endif
                                        </td>
                                    </tr>
                                    @if($cuti->status=="Diterima")
                                    <tr>
                                        <td>Cetak Surat Permohonan</td>
                                        <td>:</td>
                                        <td>
                                        <a href="/download/file_cuti/QUY89IO5TFGH3hsjsd689489ehjfcvbbuwe784Ny8756whllkhjt1234jskkmmkscditlrisrsdhtykiM0B1VCZX3546UN/{{$cuti->id}}" class="btn btn-primary btn-sm">Download <i class="fa fa-download"></i></a>
                                        </td>
                                    </tr>
                                    @else
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <!-- .animated -->
        </div>
    </div>
    <!-- /.content -->
</div>
@endsection
