@extends('/karyawan/header')
@section('title','Dashboard')
@section('container')
<div class="content">
    <!-- Animated -->
    <div class="animated fadeIn">
        <!--  Traffic  -->
        <div class="row">
            @if(session('sukses'))
            <script>
                swal("Berhasil!", "Data Berhasil Diubah!", "success");

            </script>
            @elseif(session('gagal'))
            <script>
                swal("Gagal!", "Data Password Lama Tidak Sesuai. Silahkan Cek Kembali", "error");

            </script>
            @elseif(session('edit'))
            <script>
                swal("Berhasil!", "Data Berhasil Diubah!", "success");

            </script>
            @endif
            <div class="col-lg-12">
                <div class="progress">
                    <div class="progress-bar bg-success" role="progressbar" style="width: 100%" aria-valuenow="100"
                        aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="card mt-n1">
                    <div class="card-body">
                        <h4 class="box-title">Data Diri</h4>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <img src="/foto_karyawan/foto/{{$employe->gambar}}" width="100%" alt=""
                                            srcset="">
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="progress-box progress-1">
                                            <h4 class="por-title">NIK</h4>
                                            <div class="por-txt">{{$employe->nik}}</div>
                                        </div>
                                        <div class="progress-box progress-1">
                                            <h4 class="por-title">Nama</h4>
                                            <div class="por-txt">{{$employe->name}}</div>
                                        </div>
                                        <div class="progress-box progress-1">
                                            <h4 class="por-title">Email</h4>
                                            <div class="por-txt">{{$employe->email}}</div>
                                        </div>
                                        <div class="progress-box progress-1">
                                            <h4 class="por-title">Jenis Kelamin</h4>
                                            <div class="por-txt">{{$employe->jenis_kelamin}}</div>
                                        </div>
                                        <div class="progress-box progress-1">
                                            <h4 class="por-title">Masuk Kerja</h4>
                                            <div class="por-txt">{{$employe->masuk_kerja}}</div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="progress-box progress-1">
                                            <h4 class="por-title">Jabatan</h4>
                                            <div class="por-txt">{{$employe->jabatan->jabatan}}</div>
                                        </div>
                                        <div class="progress-box progress-1">
                                            <h4 class="por-title">Departemen</h4>
                                            <div class="por-txt">{{$employe->departemen->departemen}}</div>
                                        </div>
                                        <div class="progress-box progress-1">
                                            <h4 class="por-title">Alamat</h4>
                                            <div class="por-txt">{{$employe->alamat}}</div>
                                        </div>
                                        <div class="progress-box progress-1">
                                            <h4 class="por-title">No. Handphone</h4>
                                            <div class="por-txt">{{$employe->no_hp}}</div>
                                        </div>
                                        <div class="progress-box progress-1">
                                            <h4 class="por-title">Sisa Cuti</h4>
                                            <div class="por-txt">{{$employe->sisa_cuti}} Hari</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- /.row -->
                    <div class="card-body"></div>
                </div>
            </div><!-- /# column -->
        </div>
        <!--  /Traffic -->
        <div class="clearfix"></div>
    </div>
    <!-- .animated -->
</div>
@endsection
