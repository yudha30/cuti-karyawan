@extends('/karyawan/header')
@section('title','Pengajuan Cuti')
@section('container')


<div class="content">
    <!-- Animated -->
    <div class="animated fadeIn">
        <!--  Traffic  -->
        <div class="row">
            @if(session('sukses'))
            <div class="alert alert-success col-md-12" role="alert">
                {{session('sukses')}}
            </div>
            @elseif(session('gagal'))
            <div class="alert alert-danger col-md-12" role="alert">
                {{session('gagal')}}
            </div>
            @endif
            <div class="col-lg-12">
                <div class="progress">
                    <div class="progress-bar bg-success" role="progressbar" style="width: 100%" aria-valuenow="100"
                        aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="card mt-n1">
                    <div class="card-body">
                        <h4 class="box-title">Pengajuan Cuti</h4>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="modal-body">
                                            <form action="/pengajuan_cuti/tambah" method="POST">
                                                @csrf
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">NIK</label>
                                                    <input type="text" name="nik"
                                                        class="form-control @error('nik') is-invalid @enderror" id="nik"
                                                        aria-describedby="emailHelp"
                                                        value="{{ Auth::guard('karyawans')->user()->nik }}" readonly>
                                                    @error('nik')
                                                    <span class="text-danger">{{$message}}</span>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Nama</label>
                                                    <input type="text" name="nama"
                                                        class="form-control @error('nama') is-invalid @enderror"
                                                        id="nama" value="{{ Auth::guard('karyawans')->user()->name }}"
                                                        readonly>
                                                    @error('nama')
                                                    <span class="text-danger">{{$message}}</span>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Tanggal Cuti </label>
                                                    <?php
                                                        $tgl = date('Y-m-d');
                                                        $tgl2 = date('Y-m-d', strtotime('+1 days', strtotime($tgl)));
                                                    ?>
                                                    <input type="date" name="tanggal_cuti"
                                                        class="tanggal form-control @error('tanggal_cuti') is-invalid @enderror "
                                                        id="tanggal_cuti" min="{{$tgl2}}" required>
                                                    @error('tanggal_keluar')
                                                    <span class="text-danger">{{$message}}</span>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Tanggal Masuk</label>
                                                    <div id="tanggal_masuk_filter">
                                                        <input type="date" name="tanggal_masuk"
                                                            class="tanggal form-control @error('tanggal_masuk') is-invalid @enderror"
                                                            id="tanggal_masuk" min="{{$tgl2}}" required>
                                                    </div>
                                                    @error('tanggal_masuk')
                                                    <span class="text-danger">{{$message}}</span>
                                                    @enderror
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Jumlah Cuti</label>
                                                    <input type="number" name="jumlah_cuti"
                                                        class="form-control @error('jumlah_cuti') is-invalid @enderror"
                                                        id="jumlah_cuti" aria-describedby="emailHelp"
                                                        placeholder="Enter Jumlah Cuti" readonly required>
                                                    @error('jumlah_cuti')
                                                    <span class="text-danger">{{$message}}</span>
                                                    @enderror
                                                </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Jenis Cuti</label>
                                                <select class="custom-select @error('jenis_cuti') is-invalid @enderror"
                                                    name="jenis_cuti" id="jenis_cuti" value="{{old('jenis_cuti')}}"
                                                    required="">
                                                    <option value="" selected>- Pilih Jenis Cuti -</option>
                                                    @foreach($jenis_cuti as $item)
                                                    <option value="{{$item->id}}">{{$item->jenis}}</option>
                                                    @endforeach
                                                </select>
                                                @error('jenis_cuti')
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Keperluan</label>
                                                <input type="text" name="keperluan"
                                                    class="form-control @error('keperluan') is-invalid @enderror"
                                                    id="keperluan" aria-describedby="emailHelp"
                                                    placeholder="Enter keperluan" required>
                                                @error('keperluan')
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Periode</label>
                                                <?php $tahun=date('Y'); ?>
                                                <input type="text" readonly name="periode"
                                                    class="form-control @error('periode') is-invalid @enderror"
                                                    id="periode" aria-describedby="emailHelp"
                                                    placeholder="Enter periode" value="{{$tahun}}" required>
                                                @error('periode')
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Status</label>
                                                <input type="text" name="status"
                                                    class="form-control @error('status') is-invalid @enderror"
                                                    id="status" aria-describedby="emailHelp" placeholder="Pengajuan"
                                                    value="Pengajuan" readonly>
                                                @error('status')
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <hr>
                                            <div class="text-right">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Batal</button>
                                                <button type="submit" class="btn btn-primary">Simpan</button>
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- /.row -->
                <div class="card-body"></div>
            </div>
        </div><!-- /# column -->
    </div>
    <!--  /Traffic -->
    <div class="clearfix">

    </div>
<!-- .animated -->
</div>
@endsection

@section('script')

<script>
    $('#tanggal_masuk').change(function () {
        // get data cuti dan masuk
        let cuti = document.getElementById('tanggal_cuti').value;
        let masuk = document.getElementById('tanggal_masuk').value;
        // filter
        if (cuti != null && cuti != "") {
            console.log('a');

            // get token
            var CSRF_TOKEN = $('input[name="_token"]').attr('value');
            // kirim data
            jQuery.ajax({
                type: 'post',
                url: '/karyawan/selisih/tanggal',
                data: {
                    '_token': CSRF_TOKEN,
                    cuti,
                    masuk
                },
                success: function (data) {
                    console.log(data);

                    $('#jumlah_cuti').val(data);
                }
            });
        } else {
            alert('tanggal cuti harus diisi');
            $('#tanggal_masuk').val('');
            $('#tanggal_cuti').focus();
        }
    });

</script>
@endsection
