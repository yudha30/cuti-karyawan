<style>
    .button {
        border-radius: 3px;
        box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16);
        color: #fff;
        display: inline-block;
        text-decoration: none;
        -webkit-text-size-adjust: none;
    }

    .button-blue,
    .button-primary {
        background-color: #3490dc;
        border-top: 10px solid #3490dc;
        border-right: 18px solid #3490dc;
        border-bottom: 10px solid #3490dc;
        border-left: 18px solid #3490dc;
    }
</style>
@component('mail::message')
# Notifikasi Cuti

Status Cuti Anda <strong>{{$data['status']}}</strong>.
@if($data['status'] == 'Diterima')
<p>Silahkan klik tombol dibawah untuk mendownload file cuti.</p>

<table width="100%" border="0" cellpadding="0" cellspacing="0" role="presentation">
    <tr>
        <td align="center">
            <table border="0" cellpadding="0" cellspacing="0" role="presentation">
                <tr>
                    <td>
                        <a href="http://localhost:8000/download/file_cuti/QUY89IO5TFGH3hsjsd689489ehjfcvbbuwe784Ny8756whllkhjt1234jskkmmkscditlrisrsdhtykiM0B1VCZX3546UN/{{$data['id_cuti']}}" class="button button-primary">Klik Disini</a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
@endif
<br><br>
Terimakasih,<br>
PT. Kasen Indonesia
@endcomponent
