<style>
    .button {
        border-radius: 3px;
        box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16);
        color: #fff;
        display: inline-block;
        text-decoration: none;
        -webkit-text-size-adjust: none;
    }

    .button-blue,
    .button-primary {
        background-color: #3490dc;
        border-top: 10px solid #3490dc;
        border-right: 18px solid #3490dc;
        border-bottom: 10px solid #3490dc;
        border-left: 18px solid #3490dc;
    }

    .button-red,
    .button-danger {
        background-color: #e3342f;
        border-top: 10px solid #e3342f;
        border-right: 18px solid #e3342f;
        border-bottom: 10px solid #e3342f;
        border-left: 18px solid #e3342f;
    }
</style>
@component('mail::message')
# Notifikasi Cuti

Anda memiliki pengajuan cuti baru dari <strong>{{$data['nama']}}</strong>. <br>
Berikut data cuti yang diajukan. <br>

<table width="100%" border="0" cellpadding="0" cellspacing="0" role="presentation">
    <tr>
        <td>Nama Karyawan</td>
        <td>:</td>
        <td>{{$data['nama']}}</td>
    </tr>
    <tr>
        <td>Departemen</td>
        <td>:</td>
        <td>{{$data['departemen']}}</td>
    </tr>
    <tr>
        <td>Tanggal Cuti</td>
        <td>:</td>
        <td>{{$data['tanggal_cuti']}}</td>
    </tr>
    <tr>
        <td>Tanggal Masuk</td>
        <td>:</td>
        <td>{{$data['tanggal_masuk']}}</td>
    </tr>
    <tr>
        <td>Lama Cuti</td>
        <td>:</td>
        <td>{{$data['lama_cuti']}} Hari</td>
    </tr>
    <tr>
        <td>Keperluan</td>
        <td>:</td>
        <td>{{$data['keperluan']}}</td>
    </tr>
    <tr>
        <td>Periode</td>
        <td>:</td>
        <td>{{$data['periode']}}</td>
    </tr>
    <tr>
        <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
        <td align="center" colspan="3">
            <a href="http://localhost:8000/acc/cuti/leader/{{$data['id_cuti']}}" class="button button-primary">Acc Cuti</a>
            <a href="http://localhost:8000/tolak/cuti/leader/{{$data['id_cuti']}}" class="button button-danger">Tolak Cuti</a>
        </td>
    </tr>
</table>
<br><br>
Terimakasih,<br>
PT. Kasen Indonesia
@endcomponent
