<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Route::post('dashboard', 'PageController@showdashboard');
Auth::routes();

// acc leader
Route::get('/acc/cuti/leader/{id}', 'CutiController@acc_leader');

// tolak cuti leader
Route::get('/tolak/cuti/leader/{id}','CutiController@leader_tolak_cuti');

// acc cuti Supervisor
Route::get('/acc/cuti/spv/{data}','CutiController@acc_supervisor');

// tolak cuti Supervisor
Route::get('/tolak/cuti/spv/{data}','CutiController@spv_tolak_cuti');

// notification pengajuan cuti
route::get('/karyawan/notifikasi/pengajuan_cuti', 'PengajuanCutiController@notif_pengajuan');

// download file
Route::get('/download/file_cuti/QUY89IO5TFGH3hsjsd689489ehjfcvbbuwe784Ny8756whllkhjt1234jskkmmkscditlrisrsdhtykiM0B1VCZX3546UN/{id}','CutiController@download_file_cuti');

Route::post('/auth/login', 'Auth\LoginController@cek');
route::group(['middleware' => 'auth:web'],function(){
    //
    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/karyawan', 'KaryawanController@index')->name('karyawan');
    Route::post('/karyawan/tambah', 'KaryawanController@store');
    Route::patch('/karyawan/{id}', 'KaryawanController@update');
    Route::delete('/karyawan/{id}', 'KaryawanController@destroy');

    Route::get('/detail_karyawan/{id}', 'KaryawanController@show');

    Route::get('/departemen', 'DepartemenController@index')->name('departemen');
    Route::post('/departemen/tambah', 'DepartemenController@store');
    Route::patch('/departemen/{id}', 'DepartemenController@update');
    Route::delete('/departemen/{id}', 'DepartemenController@destroy');

    // Route::get('/user', 'PageController@menuuser')->name('user');
    Route::get('/user', 'UserController@index')->name('user');
    Route::post('/user/tambah', 'UserController@store');
    Route::patch('/user/{id}', 'UserController@update');
    Route::delete('/user/{id}', 'UserController@destroy');
    Route::get('/ubah_password/{id}', 'UserController@show_ubah_password')->name('ubah_password');
    Route::post('/hrd/ubah_password/{id}', 'UbahPasswordController@update');

    Route::get('/jabatan', 'JabatanController@index')->name('jabatan');
    Route::patch('/jabatan/{id}', 'JabatanController@update');
    Route::delete('/jabatan/{id}', 'JabatanController@destroy');
    Route::post('/jabatan/tambah', 'JabatanController@store');

    // Route::get('/jenis_cuti', 'PageController@menujenis_cuti')->name('jenis_cuti');
    Route::get('/jenis_cuti', 'JenisCutiController@index')->name('jenis_cuti');
    Route::post('/jenis_cuti/tambah', 'JenisCutiController@store');
    Route::patch('/jenis_cuti/{id}', 'JenisCutiController@update');
    Route::delete('/jenis_cuti/{id}', 'JenisCutiController@destroy');

    Route::get('/cuti', 'CutiController@index')->name('cuti');
    Route::patch('/terima/cuti/{id}','CutiController@update');
    Route::delete('/tolak/cuti/{id}','CutiController@tolak_cuti');
    Route::get('/hrd/detail_cuti/{id}','CutiController@show');
    Route::post('/search/cuti','CutiController@search');

    // Laporan
    Route::get('/laporan','LaporanController@index')->name('viewLaporan');
    Route::get('/laporan/bulan/{bulan}', 'LaporanController@filter_bulan');
    Route::get('/cek', 'LaporanController@cek');
    Route::post('/cetak_laporan', 'LaporanController@cetak');
});

route::group(['middleware' => 'auth:karyawans'],function(){

    Route::prefix('karyawan')->group(function () {
        Route::get('/home_karyawan', 'HomeController@index_karyawan')->name('home_karyawan');

        // ubah password
        Route::get('/ubah_password/{id}', 'Karyawan\KaryawanController@tampil_ubah_password');
        Route::post('/ubah_password/{id}', 'Karyawan\KaryawanController@ubah_password');

        // notifikasi cuti leader
        Route::get('/notifikasi/cuti/leader/{id}', 'PengajuanCutiController@notif_leader');

        // notifikasi Cuti Supervisor
        Route::get('/notifikasi/cuti/supervisor','PengajuanCutiController@notif_supervisor');

    });


    Route::get('/home_karyawan', 'HomeController@index_karyawan')->name('home_karyawan');

    Route::get('/karyawan/data_cuti', 'PengajuanCutiController@index')->name('data_cuti');
    Route::get('/detail_cuti/{id}', 'PengajuanCutiController@show');

    Route::get('/pengajuan_cuti', 'PengajuanCutiController@create')->name('pengajuan_cuti');
    Route::post('/pengajuan_cuti/tambah', 'PengajuanCutiController@store');

    // selisih tanggal cuti
    Route::post('/karyawan/selisih/tanggal','PengajuanCutiController@selisih');

});
